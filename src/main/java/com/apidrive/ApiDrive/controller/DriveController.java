package com.apidrive.ApiDrive.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Validated
@RequestMapping("/drive")
public class DriveController {
	
	private static final Log LOG = LogFactory.getLog(DriveController.class);
	
	@Autowired
	private com.apidrive.ApiDrive.service.GoogleDriveService googleDriveServiceImp;
	
	
	@RequestMapping(value = ("/subirArchivos"), headers = ("content-type=multipart/*"), method = RequestMethod.POST)
	public Map<String, Object> subirArchivosProyecto(@RequestParam(name = "docs") MultipartFile[] multipart)
			throws IOException {

		Map<String, Object> mapRetorno = new HashMap<>();
		
		LOG.info("DRIVE CONTROLLER -- METHOD -- SUBIRARCHIVOS");

		// Obytenemos la informacion del archivo recibido
		for (int i = 0; i < multipart.length; i++) {
			LOG.info(multipart[i].getOriginalFilename());
			LOG.info(multipart[i].getContentType());

			// Obtenemos el nombre del archivo subido
			String fileName = multipart[i].getOriginalFilename();

			// File convFile = new File(file.getOriginalFilename());
			File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
			multipart[i].transferTo(convFile);
			// Obtenemos el path
			String path = convFile.getAbsolutePath();
			LOG.info("path: " + path);
			// Obtenemos el tipo de archivo
			String type = multipart[i].getContentType();
			File file = new java.io.File(path);
			LOG.info("File: " + file);
			
			LOG.info("CATCURSOSERVICEIMP -- METHOD: CARGAR ARCHIVO -- PARAMS: path " + path + " Tipo: " + type);

			com.google.api.services.drive.model.File files = googleDriveServiceImp.upLoadFile(file.getName(), path, type, "1RqJvaa9JyZa4aQ1QFSz3UQj1lLDsuNAO");
			
				LOG.info("Link: " + files.getWebContentLink());
				LOG.info("Id: " + files.getId());
				LOG.info("id del nuevo folder: " + files.getWebContentLink());
			}

		return mapRetorno;
	}
	
	
}
