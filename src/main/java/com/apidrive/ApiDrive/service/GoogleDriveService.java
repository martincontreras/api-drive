package com.apidrive.ApiDrive.service;

import java.io.OutputStream;
import java.util.Map;
import com.google.api.services.drive.model.File;

public interface GoogleDriveService {
	
	/**
	 * Servicio que permitira subir un determinado archivo a Google Drive
	 * @author Humberto Medina Hernández
	 * @param fileName String corresponde al nombre del archivo
	 * @param filePath String corresponde al Path del archivo
	 * @param mimeType String corresponde al tipo de archivo
	 * @param folderID String que corresponde a la carpeta Padre en la que se subirá el archivo
	 * @return File
	 */
	public File upLoadFile(String fileName,String filePath,String mimeType, String folderID);
	
	/**
	 * Servicio que permitirá crear una carpeta en google Drive
	 * @param nombreFolder nombre d ela nueva carpeta
	 * @param folderID String que corresponde al nombre de la carpeta padre en la que se subirá el archivo
	 * @return regresa el id del folder creado
	 */
	public File crearCarpeta(String nombreFolder, String folderID);
	
	/**
	 * Servicio para realizar la descarga de documentos de Drive
	 * @author Martin COntreras
	 * @param fileName Nombre que se le asignará al archivo si es requerido
	 * @param id Id del archivo a descargar
	 * @return
	 */
	public OutputStream downloadFiles(String id);
	
	/**
	 * @author Martin Contreras
	 * Servicio para eliminar un documento por medio de su id de la carpeta Drive y su registro en la tabla
	 * @param idDoc id de carpetadocumento
	 * @return
	 */
	public Map<String, Object> eliminarDocProyecto(int idDoc);
	
}
