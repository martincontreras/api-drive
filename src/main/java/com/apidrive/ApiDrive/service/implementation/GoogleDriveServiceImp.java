package com.apidrive.ApiDrive.service.implementation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.mortbay.log.Log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;

@Service("googleDriveServiceImp")
public class GoogleDriveServiceImp implements com.apidrive.ApiDrive.service.GoogleDriveService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GoogleDriveServiceImp.class);

	// @Value("${google.service_account_email}")
	private String serviceAccountEmail = "anzenops@anzenopsdrive.iam.gserviceaccount.com";

	// @Value("${google.application_name}")
	private String applicationName = "AnzenOpsDrive";

	//@Value("${google.service_account_key}")
	private String serviceAccountKey = "AnzenOpsDrive-3d3054725711.p12";

	// @Value("${google.folder_id}")
	// private String folderID = "1E372j5Ry7frlc95DjIqiF-dAkDRPiEKI";

	public Drive getDriveService() {
		Drive service = null;
		try {

			URL resource = GoogleDriveServiceImp.class.getResource("/" + this.serviceAccountKey);
			java.io.File key = Paths.get(resource.toURI()).toFile();
			HttpTransport httpTransport = new NetHttpTransport();
			JacksonFactory jsonFactory = new JacksonFactory();

			GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
					.setJsonFactory(jsonFactory).setServiceAccountId(serviceAccountEmail)
					.setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE))
					.setServiceAccountPrivateKeyFromP12File(key).build();
			service = new Drive.Builder(httpTransport, jsonFactory, credential).setApplicationName(applicationName)
					.setHttpRequestInitializer(credential).build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());

		}

		return service;

	}

	@Override
	public File upLoadFile(String fileName, String filePath, String mimeType, String folderID) {

		File file = new File();
		try {
			java.io.File fileUpload = new java.io.File(filePath);
			com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
			fileMetadata.setMimeType(mimeType);
			fileMetadata.setName(fileName);
			fileMetadata.setParents(Collections.singletonList(folderID));
			com.google.api.client.http.FileContent fileContent = new FileContent(mimeType, fileUpload);
			file = getDriveService().files().create(fileMetadata, fileContent)
					.setFields("id,webContentLink,webViewLink").execute();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return file;
	}

	@Transactional
	@Override
	public File crearCarpeta(String nombreFolder, String folderID) {
		Log.info("Nombre del Folder: " + nombreFolder);
		Log.info("folder ID Padre: " + folderID);
		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		Log.info("folder ID Padre: " + fileMetadata);

		File file = null;
		try {
			fileMetadata.setName(nombreFolder);
			fileMetadata.setMimeType("application/vnd.google-apps.folder");
			fileMetadata.setParents(Collections.singletonList(folderID));

			file = getDriveService().files().create(fileMetadata).setFields("id,webViewLink").execute();
			System.out.println("Folder ID: " + file.getId());
			System.out.println("Folder view link: " + file.getWebViewLink());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return file;
	}

	@Override
	public OutputStream downloadFiles(String id) {
		OutputStream outputStream = new ByteArrayOutputStream();

		try {
			getDriveService().files().get(id).executeMediaAndDownloadTo(outputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return outputStream;
	}

	@Transactional
	@Override
	public Map<String, Object> eliminarDocProyecto(int idDoc) {
		Map<String, Object> mapRetorno = new HashMap<String, Object>();
//		int estatus = 0;
//		String descripcion = "";
//		Log.info("GOOGLEDRIVESERVICE--ELIMINARDOCPROYECTO");
//		CarpetaDocumento carpetaDocumento = carpetaDocumentoRepoImp.findById(idDoc);
//		Log.info("PROYECTO CARPETA_: " + carpetaDocumento.getIdDocumentoDrive() );
//		deleteFile(carpetaDocumento.getIdDocumentoDrive());
//
//		int rs = carpetaDocumentoRepoImp.deleteRow(idDoc);
//		if (rs==1)
//		{
//			estatus =1;
//		}
//
//		// SETEO DESCRIPCIÓN ESTATUS PARA LA VALIDACION 1
//		if (estatus == 0) {
//			descripcion = "PETICION FALLIDA";
//		}
//		if (estatus == 1) {
//			descripcion = "PETICION EXITOSA";
//		}
//
//		// SETEO DE MAP ESTATUS
//		Map<String, Object> mapEstatus = new HashMap<String, Object>();
//		mapEstatus.put("estatus", estatus);
//		mapEstatus.put("descripcion", descripcion);
//
//		// SETEO DE MAP RETORNO
//		mapRetorno.put("estatus", mapEstatus);

		return mapRetorno;
	}

}
